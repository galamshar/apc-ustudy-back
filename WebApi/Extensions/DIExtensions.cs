﻿using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;
using UPlanner.Application.Auth.Services;
using UPlanner.Infrastructure.Auth.Services;

namespace UPlanner.WebApi.Extensions
{
    public static class DIExtensions
    {
        public static IServiceCollection AddMediatorHandlers(this IServiceCollection services, Assembly assembly)
        {
            services.AddMediatR(typeof(Startup));

            var handlerClasses = assembly.DefinedTypes
                .Select(t => t.GetTypeInfo())
                .Where(t => t.IsClass && !t.IsAbstract);

            foreach (var handlerClass in handlerClasses)
            {
                var handlerInterfaces = handlerClass.ImplementedInterfaces
                    .Select(t => t.GetTypeInfo())
                    .Where(t => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(IRequestHandler<,>));

                foreach (var handlerInterface in handlerInterfaces)
                {
                    services.AddTransient(handlerInterface.AsType(), handlerClass.AsType());
                }
            }

            return services;
        }

        public static IServiceCollection AddSecurityServices(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddHttpContextAccessor();
            services.Configure<DefaultTokenFactoryOptions>(configuration.GetSection("Security:Token"));
            services.AddScoped<ITokenFactory, DefaultTokenFactory>();
            services.AddTransient<IPasswordGenerator, DefaultPasswordGenerator>();
            services.AddScoped<ISecurityContext, DefaultSecurityContext>();
            services.AddScoped<IPasswordHasher, DefaultPasswordHasher>();

            return services;
        }

        public static IServiceCollection AddAuthenticationSystem(
                  this IServiceCollection services,
                  IConfiguration configuration)
        {
            services
                .AddAuthentication(opts =>
                {
                    opts.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    opts.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(opts =>
                {
                    var jwt = configuration.GetSection("Security:Token").Get<DefaultTokenFactoryOptions>();

                    opts.RequireHttpsMetadata = false;

                    opts.TokenValidationParameters = new()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,

                        ValidIssuer = jwt.Issuer,
                        ValidAudience = jwt.Audience,

                        RequireSignedTokens = true,
                        IssuerSigningKey = jwt.CreateKey(),

                        NameClaimType = "sub",
                        RoleClaimType = "role",
                    };

                    opts.MapInboundClaims = false;
                });

            services.AddAuthorization(opts =>
            {
                opts.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build();
            });

            return services;
        }
    }
}
