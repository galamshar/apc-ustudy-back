﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UPlanner.Application.Auth.Services;
using UPlanner.Application.Planning.Models;
using UPlanner.Application.Planning.UseCases;

namespace UPlanner.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ClassroomsController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ISecurityContext _securityContext;

        public ClassroomsController(IMediator mediator, ISecurityContext securityContext)
        {
            _mediator = mediator;
            _securityContext = securityContext;
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> AddClassroom([FromBody] AddClassroomUseCase useCase)
        {
            await _mediator.Send(useCase);
            return NoContent();
        }

        [Authorize]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetClassroomsUseCaseOutput))]
        public async Task<IActionResult> GetClassrooms([FromQuery] GetClassroomsUseCase useCase)
        {
            var senderId = _securityContext.UserId;
            var user = await _mediator.Send(new GetUserUseCase(senderId));
            useCase.RegionId = user.User.RegionId;
            return Ok(await _mediator.Send(useCase));
        }

        [Authorize]
        [HttpGet("get")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(ClassroomDto))]
        public async Task<IActionResult> GetClassroom([FromQuery] GetClassroomUseCase useCase)
        {
            return Ok(await _mediator.Send(useCase));
        }
    }
}
