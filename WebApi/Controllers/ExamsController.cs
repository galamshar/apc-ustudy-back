﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using UPlanner.Application.Planning.UseCases;
using UPlanner.Domain.Models;

namespace UPlanner.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ExamsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ExamsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> AddExam([FromBody] AddExamUseCase useCase)
        {
            await _mediator.Send(useCase);
            return NoContent();
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetExamsUseCase))]
        [Authorize]
        public async Task<IActionResult> GetExams()
        {
            var exams = await _mediator.Send(new GetExamsUseCase());
            return Ok(exams);
        }

    }
}
