﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UPlanner.Application.Auth.Services;
using UPlanner.Application.Auth.UseCases;
using UPlanner.Application.Planning.UseCases;

namespace UPlanner.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ISecurityContext _securityContext;

        public UsersController(IMediator mediator, ISecurityContext securityContext)
        {
            _mediator = mediator;
            _securityContext = securityContext;
        }

        //[Authorize(Roles = "Administrator")]
        [HttpPost("sign-up")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(SignUpUseCaseOutput))]
        public async Task<IActionResult> SignUp([FromBody] SignUpUseCase useCase)
        {
            var response = await _mediator.Send(useCase);

            return Ok(response);
        }

        [Authorize]
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetUserUseCase))]
        public async Task<IActionResult> GetUser()
        {
            var userId = _securityContext.UserId;
            var response = await _mediator.Send(new GetUserUseCase(userId));
            return Ok(response);
        }
    }
}
