﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;
using UPlanner.Application.Auth.Services;
using UPlanner.Application.Planning.UseCases;

namespace UPlanner.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProposalsController : ControllerBase
    {
        private readonly ISecurityContext _securityContext;
        private readonly IMediator _mediator;

        public ProposalsController(
            IMediator mediator,
            ISecurityContext securityContext)
        {
            _mediator = mediator;
            _securityContext = securityContext;
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> AddProposal([FromBody] AddPlanProposalUseCase useCase)
        {
            useCase.SenderId = _securityContext.UserId;
            await _mediator.Send(useCase);
            return NoContent(); 
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetProposalsUseCaseOutput))]
        public async Task<IActionResult> GetProposals([FromQuery] GetProposalsUseCase useCase)
        {
            return Ok(await _mediator.Send(useCase));
        }

        [HttpGet("approve")]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> ApproveProposal([FromQuery] ApproveProposalUseCase useCase)
        {
            await _mediator.Send(useCase);
            return NoContent();
        }

        [HttpGet("reject")]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> RejectProposal([FromQuery] RejectProposalUseCase useCase)
        {
            await _mediator.Send(useCase);
            return NoContent();
        }

        [HttpGet("region")]
        [Authorize]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(GetProposalsUseCaseOutput))]
        public async Task<IActionResult> GetProposalsByUserRegion([FromQuery] GetProposalsUseCase useCase)
        {
            var senderId = _securityContext.UserId;
            var user = await _mediator.Send(new GetUserUseCase(senderId));
            useCase.RegionId = user.User.RegionId;
            return Ok(await _mediator.Send(useCase));
        }
    }
}
