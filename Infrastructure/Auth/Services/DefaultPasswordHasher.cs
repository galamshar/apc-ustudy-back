﻿using UPlanner.Application.Auth.Services;

namespace UPlanner.Infrastructure.Auth.Services
{
    public class DefaultPasswordHasher : IPasswordHasher
    {
        public string Hash(string password)
        {
            return password;
        }

        public bool Verify(string password, string hash)
        {
            return password == hash;
        }
    }
}
