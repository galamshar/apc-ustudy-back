﻿using System;
using System.Collections.Generic;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.Models
{
    public class PlanProposalDto
    {
        public Guid Id { get; set; }
        public Guid SenderId { get; set; }
        public Guid ClassroomId { get; set; }
        public string OfficeName { get; set; }
        public List<PlanActionDto> Actions { get; set; }
        public PlanProposalStatus Status { get; set; }
    }
}
