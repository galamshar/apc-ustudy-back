﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.Exceptions
{
    public class NotEnoughPlacesException : Exception
    {
        public NotEnoughPlacesException(
            Classroom classroom,

            string message = null,
            Exception innerException = null) : base(message, innerException)
        {
            Classroom = classroom ?? throw new ArgumentNullException(nameof(classroom));

        }

        public Classroom Classroom { get; }
    }
}
