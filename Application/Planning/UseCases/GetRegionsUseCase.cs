﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Common.Extensions;
using UPlanner.Application.Common.Models;
using UPlanner.Application.Planning.Models;
using UPlanner.Domain.Data;

namespace UPlanner.Application.Planning.UseCases
{
    public class GetRegionsUseCase : IRequest<GetRegionsUseCaseOutput>
    {
        public PageRequest PageRequest { get; set; }
    }

    public class GetRegionsUseCaseHandler : IRequestHandler<GetRegionsUseCase, GetRegionsUseCaseOutput>
    {
        private readonly PlannerContext _db;

        public GetRegionsUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        public async Task<GetRegionsUseCaseOutput> Handle(GetRegionsUseCase request, CancellationToken cancellationToken)
        {
            var page = await _db.Regions
                .Select(r => new RegionDto { Id = r.Id, Name = r.Name })
                .ToPageAsync(request.PageRequest ?? PageRequest.Default(), cancellationToken);

            return new() { Page = page };
        }
    }

    public class GetRegionsUseCaseOutput
    {
        public PageResponse<RegionDto> Page { get; set; }
    }
}
