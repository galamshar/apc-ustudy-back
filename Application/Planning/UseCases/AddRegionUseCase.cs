﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class AddRegionUseCase : IRequest
    {
        public string Name { get; set; }
    }

    public class AddRegionUseCaseHandler : AsyncRequestHandler<AddRegionUseCase>
    {
        private readonly PlannerContext _db;

        public AddRegionUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        protected override async Task Handle(AddRegionUseCase request, CancellationToken cancellationToken)
        {
            var region = new Region { Id = Guid.NewGuid(), Name = request.Name };

            _db.Regions.Add(region);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
