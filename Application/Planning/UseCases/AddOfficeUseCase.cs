﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class AddOfficeUseCase : IRequest
    {
        public Guid RegionId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }

    public class AddOfficeUseCaseHandler : AsyncRequestHandler<AddOfficeUseCase>
    {
        private readonly PlannerContext _db;

        public AddOfficeUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        protected override async Task Handle(AddOfficeUseCase request, CancellationToken cancellationToken)
        {
            var office = new Office
            {
                Id = Guid.NewGuid(),
                RegionId = request.RegionId,
                Name = request.Name,
                Address = request.Address
            };

            _db.Offices.Add(office);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
