﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Common.Extensions;
using UPlanner.Application.Common.Models;
using UPlanner.Application.Planning.Models;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class GetProposalsUseCase : IRequest<GetProposalsUseCaseOutput>
    {
        public Guid? RegionId { get; set; }
        public PageRequest PageRequest { get; set; }
    }

    public class GetProposalsUseCaseHandler : IRequestHandler<GetProposalsUseCase, GetProposalsUseCaseOutput>
    {
        private readonly PlannerContext _db;

        public GetProposalsUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        public async Task<GetProposalsUseCaseOutput> Handle(GetProposalsUseCase request, CancellationToken cancellationToken)
        {
            IQueryable<PlanProposal> query = _db.Proposals;

            if (request.RegionId.HasValue && request.RegionId != Guid.Parse("00000000-0000-0000-0000-000000000001"))
            {
                query = query.Where(o => o.Classroom.Office.RegionId == request.RegionId);
            }

            var page = await query
                .Select(p => new PlanProposalDto
                {
                    Id = p.Id,
                    SenderId = p.SenderId,
                    Status = p.Status,
                    ClassroomId = p.ClassroomId,
                    OfficeName = p.Classroom.Office.Name,
                    Actions = p.Actions.Select(a => new PlanActionDto
                    {
                        Type = a.Type,
                        Date = a.Date,
                        ExamId = a.ExamId,
                        People = a.People,
                        ExamName = a.Exam.Name,
                        IsMixable = a.Exam.Mixable
                    }).ToList()
                })
                .ToPageAsync(request.PageRequest ?? PageRequest.Default(), cancellationToken);

            return new() { Page = page };
        }
    }

    public class GetProposalsUseCaseOutput
    {
        public PageResponse<PlanProposalDto> Page { get; set; }
    }
}
