﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Planning.Models;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class GetClassroomUseCase : IRequest<ClassroomDto>
    {
        public Guid ClassroomId { get; set; }
        public DateTimeOffset DateTo { get; set; }
    }

    public class GetClassroomUseCaseHandler : IRequestHandler<GetClassroomUseCase, ClassroomDto>
    {
        private readonly PlannerContext _db;

        public GetClassroomUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        public async Task<ClassroomDto> Handle(GetClassroomUseCase request, CancellationToken cancellationToken)
        {
            var classroom = _db.Classrooms.Include(c => c.Proposals).FirstOrDefault(c => c.Id == request.ClassroomId);
            var compareDate = DateTimeOffset.UtcNow;
            if(request.DateTo != null)
            {
                compareDate = request.DateTo;
            }

            ClassroomDto classroomDto = new()
            {
                Id = classroom.Id,
                OfficeId = classroom.OfficeId,
                Capacity = classroom.Capacity,
                TakenPlaces = classroom.Proposals.Where(p => p.Status == PlanProposalStatus.Approved)
                                         .Select(p => new { Selected = p.Actions.Where(a => a.Date < compareDate).Select(ac => ac.Type == ActionType.AddGroup ? ac.People * -1 : ac.People).Sum() })
                                         .Sum(n => n.Selected)
            };

            return classroomDto;

        }
    }
}
