﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class ApproveProposalUseCase : IRequest
    {
        public Guid ProposalId { get; set; }
    }

    public class ApproveProposalUseCaseHandler : AsyncRequestHandler<ApproveProposalUseCase>
    {
        private readonly PlannerContext _db;

        public ApproveProposalUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        protected override async Task Handle(ApproveProposalUseCase request, CancellationToken cancellationToken)
        {
            var proposal = await _db.Proposals
                .FindAsync(keyValues: new object[] { request.ProposalId }, cancellationToken: cancellationToken);

            proposal.Status = PlanProposalStatus.Approved;

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
