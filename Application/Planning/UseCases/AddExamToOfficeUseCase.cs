﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Planning.Exceptions;
using UPlanner.Domain.Data;

namespace UPlanner.Application.Planning.UseCases
{
    public class AddExamToOfficeUseCase:IRequest
    {
        public Guid OfficeId {  get; set; }
        public Guid ExamId {  get; set; }
    }

    public class AddExamToOfficeUseCaseHandler : AsyncRequestHandler<AddExamToOfficeUseCase>
    {
        private readonly PlannerContext _db;

        public AddExamToOfficeUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        protected override async Task Handle(AddExamToOfficeUseCase request, CancellationToken cancellationToken)
        {
            var exam = _db.Exams.Include(e => e.Offices).FirstOrDefault(e => e.Id == request.ExamId);
            var office = _db.Offices.Include(o => o.Exams).FirstOrDefault(o => o.Id == request.OfficeId);
            if (office.Exams.Contains(exam))
            {
                throw new ResponseException() { Status = 400, Value = "This exam already allowed in this office" };
            }
            office.Exams.Add(exam);
            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
