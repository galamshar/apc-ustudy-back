﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Planning.Models;
using UPlanner.Domain.Data;

namespace UPlanner.Application.Planning.UseCases
{
    public class GetUserUseCase : IRequest<GetUserUseCaseOutput>
    {
        public Guid UserId { get; set; }

        public GetUserUseCase(Guid userId)
        {
            UserId = userId;
        }
    }

    public class GetUserUseCaseHandler : IRequestHandler<GetUserUseCase, GetUserUseCaseOutput>
    {
        private readonly PlannerContext _db;

        public GetUserUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        public async Task<GetUserUseCaseOutput> Handle(GetUserUseCase request, CancellationToken cancellationToken)
        {
            var dbUser = _db.Users.Include(u => u.Region).FirstOrDefault(u => u.Id == request.UserId);
            var user = new UserDto()
            {
                Id = dbUser.Id,
                Login = dbUser.Login,
                Role = dbUser.Role.ToString(),
                RegionId = dbUser.RegionId,
                RegionName = dbUser.Region.Name
            };
            return new() { User = user };
        }
    }

    public class GetUserUseCaseOutput
    {
        public UserDto User { get; set; }
    }
}
