﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Common.Extensions;
using UPlanner.Application.Common.Models;
using UPlanner.Application.Planning.Models;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class GetClassroomsUseCase : IRequest<GetClassroomsUseCaseOutput>
    {
        public Guid? OfficeId { get; set; }
        public Guid RegionId { get; set; }
        public PageRequest PageRequest { get; set; }
        public DateTimeOffset DateTo { get; set; }
    }

    public class GetClassroomsUseCaseHandler : IRequestHandler<GetClassroomsUseCase, GetClassroomsUseCaseOutput>
    {
        private readonly PlannerContext _db;
        public PageRequest PageRequest { get; set; }

        public GetClassroomsUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        public async Task<GetClassroomsUseCaseOutput> Handle(GetClassroomsUseCase request, CancellationToken cancellationToken)
        {
            IQueryable<Classroom> query = _db.Classrooms.Include(c => c.Proposals).Where(c => c.Office.RegionId == request.RegionId);

            if (request.RegionId == Guid.Parse("00000000-0000-0000-0000-000000000001"))
            {
                query = _db.Classrooms;
            }

            if (request.OfficeId.HasValue)
            {
                query = query.Where(classroom => classroom.OfficeId == request.OfficeId);
            }

            var compareDate = DateTimeOffset.UtcNow;
            if (request.DateTo != null)
            {
                compareDate = request.DateTo;
            }
            var page = await query.Select(c =>
            new ClassroomDto()
            {
                Id = c.Id,
                OfficeId = c.OfficeId,
                Capacity = c.Capacity,
                TakenPlaces = c.Proposals.Where(p => p.Status == PlanProposalStatus.Approved)
                                         .Select(p => new { Selected = p.Actions.Where(a => a.Date < compareDate).Select(ac => ac.Type == ActionType.AddGroup ? ac.People * -1 : ac.People).Sum() })
                                         .Sum(n => n.Selected)

            }).ToPageAsync(request.PageRequest ?? PageRequest.Default(), cancellationToken);

            return new() { Page = page };
        }
    }

    public class GetClassroomsUseCaseOutput
    {
        public PageResponse<ClassroomDto> Page { get; set; }
    }
}
