﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Application.Planning.Exceptions;
using UPlanner.Application.Planning.Models;
using UPlanner.Domain.Data;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Planning.UseCases
{
    public class AddPlanProposalUseCase : IRequest
    {
        public Guid SenderId { get; set; }
        public Guid ClassroomId { get; set; }
        public List<PlanActionDto> Actions { get; set; }
    }

    public class AddPlanProposalUseCaseHandler : AsyncRequestHandler<AddPlanProposalUseCase>
    {
        private readonly PlannerContext _db;

        public AddPlanProposalUseCaseHandler(PlannerContext db)
        {
            _db = db;
        }

        protected override async Task Handle(AddPlanProposalUseCase request, CancellationToken cancellationToken)
        {
            var proposal = new PlanProposal
            {
                SenderId = request.SenderId,

                Status = PlanProposalStatus.Created,
                ClassroomId = request.ClassroomId,

                Actions = request.Actions.Select(a => new PlanAction
                {
                    ExamId = a.ExamId,
                    People = a.People,
                    Type = a.Type,
                    Date = a.Date
                }).ToList()
            };

            // TODO: Валидация
            var exam = _db.Exams.FirstOrDefault(e => e.Id == proposal.Actions.FirstOrDefault().ExamId);
            var allActionsClassroom = _db.Classrooms.Include(pr => pr.Proposals).ThenInclude(pr => pr.Actions).FirstOrDefault(c => c.Id == proposal.ClassroomId).Proposals.SelectMany(p => p.Actions).ToList();
            var start = proposal.Actions.OrderBy(ac => ac.Date).Select(a => a.Date).FirstOrDefault();
            var end = proposal.Actions.OrderByDescending(ac => ac.Date).Select(a => a.Date).FirstOrDefault();
            var exams = allActionsClassroom.Where(a => a.Date >= start && a.Date <= end).Select(ac => ac.Exam).ToList();
            var takenPlaces = allActionsClassroom.Where(a => a.Date >= start && a.Date <= end && a.Type == ActionType.AddGroup).Sum(action => action.People);
            var classroom = _db.Classrooms.Include(cl => cl.Office.Exams).FirstOrDefault(c => c.Id == request.ClassroomId);
            var availablePlaces = classroom.Capacity - takenPlaces;
            var requestedPlaces = proposal.Actions.FirstOrDefault(p => p.Type == ActionType.AddGroup).People;
            var examAllowed = classroom.Office.Exams.Contains(exam);
            var proposalExams = proposal.Actions.Select(a => a.Exam);

/*            if (!exams.All(exam => proposalExams.Contains(exam)) || (!exam.Mixable && !exams.All(e => e.Mixable)))
            {
                throw new ResponseException() { Status = (int)HttpStatusCode.BadRequest, Value = "Current exams in classroom or requested exam not mixable" };
            }*/
            if (!examAllowed)
            {
                throw new ResponseException() { Status = (int)HttpStatusCode.BadRequest, Value = "Exam not allowed in this office" };
            }
            if (!proposal.Actions.All(a => a.People > 0))
            {
                throw new ResponseException() { Status = (int)HttpStatusCode.BadRequest, Value = "Requested place count can`t be lower than zero" };

            }
            if (start < DateTimeOffset.UtcNow && start < end && end < DateTimeOffset.UtcNow)
            {
                throw new ResponseException() { Status = (int)HttpStatusCode.BadRequest, Value = "Start or End date not actual" };

            }
            if (availablePlaces < requestedPlaces)
            {
                throw new ResponseException() { Status = (int)HttpStatusCode.BadRequest, Value = "Not enough places in class" };

            }

            _db.Proposals.Add(proposal);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
