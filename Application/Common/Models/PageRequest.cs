﻿namespace UPlanner.Application.Common.Models
{
    public class PageRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public static PageRequest Default() => new() { PageIndex = 0, PageSize = 100 };
    }
}
