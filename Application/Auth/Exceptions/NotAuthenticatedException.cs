﻿using System;

namespace UPlanner.Application.Auth.Exceptions
{
    public class NotAuthenticatedException : Exception
    {
        public NotAuthenticatedException()
        {
        }
    }
}
