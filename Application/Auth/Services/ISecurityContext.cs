﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UPlanner.Domain.Models;

namespace UPlanner.Application.Auth.Services
{
    public interface ISecurityContext
    {
        bool UserExists { get; }

        Guid UserId { get; }

        Task<User> GetUserAsync(CancellationToken cancellationToken = default);
    }
}
