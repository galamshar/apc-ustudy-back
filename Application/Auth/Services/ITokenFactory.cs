﻿using UPlanner.Domain.Models;

namespace UPlanner.Application.Auth.Services
{
    public interface ITokenFactory
    {
        string CreateToken(User user);
    }
}
