﻿using System.Reflection;

namespace UPlanner.Application
{
    public static class ApplicationLayerAssemblyPointer
    {
        public static Assembly Pointer { get; } = typeof(ApplicationLayerAssemblyPointer).Assembly;
    }
}
