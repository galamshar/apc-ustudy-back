﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UPlanner.Domain.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Exams",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mixable = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Offices",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Offices_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Login = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Role = table.Column<int>(type: "int", nullable: false),
                    RegionId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValue: new Guid("00000000-0000-0000-0000-000000000001"))
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Classrooms",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Capacity = table.Column<int>(type: "int", nullable: false),
                    OfficeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Classrooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Classrooms_Offices_OfficeId",
                        column: x => x.OfficeId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExamOffice",
                columns: table => new
                {
                    ExamsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OfficesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamOffice", x => new { x.ExamsId, x.OfficesId });
                    table.ForeignKey(
                        name: "FK_ExamOffice_Exams_ExamsId",
                        column: x => x.ExamsId,
                        principalTable: "Exams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExamOffice_Offices_OfficesId",
                        column: x => x.OfficesId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Proposals",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SenderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    ClassroomId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proposals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Proposals_Classrooms_ClassroomId",
                        column: x => x.ClassroomId,
                        principalTable: "Classrooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Proposals_Users_SenderId",
                        column: x => x.SenderId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Actions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    People = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    ProposalId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExamId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Date = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Actions_Exams_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Actions_Proposals_ProposalId",
                        column: x => x.ProposalId,
                        principalTable: "Proposals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Exams",
                columns: new[] { "Id", "Mixable", "Name" },
                values: new object[,]
                {
                    { new Guid("42c11ef1-46f5-48e0-a7b4-d2c3e8a1f8e6"), false, "ЕНТ" },
                    { new Guid("ee0a925e-b2d7-4884-9bcb-32ed37a7f581"), false, "НКТ" },
                    { new Guid("eca0a809-a4ea-49b1-81fc-93ab3bd41a0c"), true, "Вступительные в магистратуру" },
                    { new Guid("4ab481a0-af15-4f8b-b301-3108952de722"), true, "Вступительные в докторантуру" }
                });

            migrationBuilder.InsertData(
                table: "Regions",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("24b24037-afe3-4625-97b9-42e3ff85006b"), "Нур-Султан" },
                    { new Guid("b97a2348-a405-458b-9700-ee7c3662bb00"), "Алматы" },
                    { new Guid("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173"), "Туркестанская" },
                    { new Guid("6b027d06-11f4-4799-a834-694a8cb8885a"), "СКО" },
                    { new Guid("a0523143-342a-4be9-81cc-bbe5dd273b41"), "Павлодарская" },
                    { new Guid("cb267347-010b-46c5-a2e9-6eb02c8d3ca0"), "Мангыстауская" },
                    { new Guid("97b90ff3-935e-4cd9-9ca4-57504fa12255"), "Кызылординская" },
                    { new Guid("a9091b3e-61d0-4166-8450-f2c87accbf93"), "Костанайская" },
                    { new Guid("ed62791f-83c5-4219-9dec-19f21be1605f"), "ЗКО" },
                    { new Guid("b303ac57-8506-4a4d-83c4-f649e2170e3e"), "Шымкент" },
                    { new Guid("95069bca-bf8c-4371-b070-b44c288c248f"), "Жамбылская" },
                    { new Guid("b99e5dab-18e7-496d-8d00-aab6ebe1754c"), "ВКО" },
                    { new Guid("9b7648ca-3989-4ddd-9356-00c20324f6b8"), "Атырауская" },
                    { new Guid("017567f4-e687-446d-a2d7-0f2df5798200"), "Акмолинская" },
                    { new Guid("ea70f091-1d51-4477-9d4f-8879103bb8f7"), "Актюбинская" },
                    { new Guid("5fe90a81-e723-43b7-9613-2fdcb9f4585e"), "Алматинская" },
                    { new Guid("00000000-0000-0000-0000-000000000001"), "General" },
                    { new Guid("c1e223eb-8ee9-4bff-a6ae-1dade6d20039"), "Карагандинская" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Login", "PasswordHash", "Role" },
                values: new object[] { new Guid("41dfca6b-2c91-4616-bc82-3fe04dc7ae43"), "admin", "qwerty123", 0 });

            migrationBuilder.InsertData(
                table: "Offices",
                columns: new[] { "Id", "Address", "Name", "RegionId" },
                values: new object[,]
                {
                    { new Guid("35b7598d-28f1-4d12-aaa1-95aca831950c"), "Северно - западный жилой район ул.Қабанбай батыра 180(25 школа)", "Талдыкорган", new Guid("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    { new Guid("88b4b9bd-82f2-49ba-b168-850809a54cbe"), "ул Тажибаева,18а", "Кызылорда", new Guid("97b90ff3-935e-4cd9-9ca4-57504fa12255") },
                    { new Guid("16b937e8-91b7-4b4b-9a77-e55e615d9c54"), "Балқы Базар 19 А,Тәйімбет Көмекбаев атындағы №250 мектеп - лицей", "Кармакшинский район, п.Жосалы", new Guid("97b90ff3-935e-4cd9-9ca4-57504fa12255") },
                    { new Guid("5b0e85d0-f832-4d5e-8887-8d98ee5190de"), " Үрзімат Мадиев 51,№110 мектеп - лицей", "п.Жанакорган", new Guid("97b90ff3-935e-4cd9-9ca4-57504fa12255") },
                    { new Guid("1d184008-f44c-46a4-99ec-762ba4ac5ee1"), "Ж.Нұрмағанбетұлы көшесі 128,Е.Бозғұлов атындағы №249 мектеп - лицей", "Казалинский р-н, Кент Айтебе би", new Guid("97b90ff3-935e-4cd9-9ca4-57504fa12255") },
                    { new Guid("ee8c293b-23f0-4f0c-8d96-fc8dc67c6e3c"), "15-й микрорайон, 31    (IT  мектеп-лицей вход с торца, левое крыло)", "Актау", new Guid("cb267347-010b-46c5-a2e9-6eb02c8d3ca0") },
                    { new Guid("0a667344-4d06-497b-b224-d31dac9cb5e7"), "Школу им Ы.Алтынсарина по адресу Косай ата 15", "Бейнеуский р-н, г.Бейнеу", new Guid("cb267347-010b-46c5-a2e9-6eb02c8d3ca0") },
                    { new Guid("9e8b87b9-0eef-4ae5-8a00-69b6fe84990c"), "​Отырар, 65а Арай м - н", "г.Жанаозен", new Guid("cb267347-010b-46c5-a2e9-6eb02c8d3ca0") },
                    { new Guid("b5ce5731-2299-4090-9b61-df82017a24f5"), "ул.Толстого 99", "Павлодар", new Guid("a0523143-342a-4be9-81cc-bbe5dd273b41") },
                    { new Guid("1a0e2a97-a8a2-45ee-accf-2a04714cf3a2"), "ул. Энергетиков 54А", "г.Экибастуз", new Guid("a0523143-342a-4be9-81cc-bbe5dd273b41") },
                    { new Guid("bbb4d447-5df7-4622-9194-06d900ce7934"), "ул. Ауэзова, 6", "г. Аксу", new Guid("a0523143-342a-4be9-81cc-bbe5dd273b41") },
                    { new Guid("ed58427d-f5b3-46e2-b83d-d9bc83d3ef75"), " ул. Конституции Казахстана, 60", "Петропавловск", new Guid("6b027d06-11f4-4799-a834-694a8cb8885a") },
                    { new Guid("27936879-a81b-4970-9633-11d41777db88"), "ул. Ауельбекова 2А", "р-н Габита Мусрепова г. Новоишимское", new Guid("6b027d06-11f4-4799-a834-694a8cb8885a") },
                    { new Guid("36a26cbd-5ae0-4356-936f-f6cdb030ea1f"), " ул Конституция Казахстана 261.(Тайыншинский колледж агробизнеса)", "г.Тайынша", new Guid("6b027d06-11f4-4799-a834-694a8cb8885a") },
                    { new Guid("2f625dc6-592c-4f76-a771-59d01669e3da"), "1 мкр 24В (педколледж)", "Туркестан", new Guid("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    { new Guid("80ec8904-8b03-4a15-9684-8a8bf48a28af"), "райцентр Темирлан, ул. Қажымұқан 77/2 (жаңа мектеп)", "Ордабасы ", new Guid("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    { new Guid("4b8c3b76-a91f-479f-9ef3-f25ca981e218"), "Әйтеке би көшесі. 3А «Оңтүстік Қазақстан индустриалды-инновациялық колледжі»", "Сайрам, Ақсу кенті", new Guid("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    { new Guid("cde7f22c-74a2-4db1-a26f-9ba2fe016779"), "Шәмші гүлзары 21а (Қапланбек жоғары аграрлық-техникалық колледжі) ", "Сарыағаш", new Guid("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    { new Guid("d3c368bc-cc3c-48ec-a227-dce8a1a65913"), "Тоқпанбетов 1 а (Нәзір Төрекулов атындағы  IT мектеп лицейі)", "Шолаққорған", new Guid("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    { new Guid("04097107-c061-4d50-874d-f81d0c25db80"), "КГУ СШ №8 им.Алпамыс батыра. ул.М.Озтурик №13  ", "Жетисай", new Guid("2243dd6f-8eda-4f3a-8fec-7f6ec7fea173") },
                    { new Guid("ea5ab2e0-f21e-483e-9181-abe8ffe23b77"), "Проспект Аль-Фараби, 71", "Алматы", new Guid("b97a2348-a405-458b-9700-ee7c3662bb00") },
                    { new Guid("8e3ef246-7706-4a5f-afa8-b2feb935f64d"), "Микрорайон Коктем-1, 26а", "Алматы 2", new Guid("b97a2348-a405-458b-9700-ee7c3662bb00") },
                    { new Guid("b238d135-dd85-47fa-9764-a2e9172f5f97"), "Тауелсиздик 52", "Есильский р-н", new Guid("24b24037-afe3-4625-97b9-42e3ff85006b") },
                    { new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25"), "ул.Терешкова 14 А", "Шымкент", new Guid("b303ac57-8506-4a4d-83c4-f649e2170e3e") },
                    { new Guid("a09c37a8-74f4-4a82-8282-6fc1ba2cb4ef"), "ул.Мауленова 26 (Аркалыкский многопрофильный колледж Казпотребсоюза)", " г.Аркалык    ", new Guid("a9091b3e-61d0-4166-8450-f2c87accbf93") },
                    { new Guid("e6323325-bd98-4306-b4c3-ca8cea161ca2"), "6 микрорайон, дом 56", "г.Лисаковск", new Guid("a9091b3e-61d0-4166-8450-f2c87accbf93") },
                    { new Guid("c34ccf6a-d27e-4a17-9868-99b855abed0b"), "ул.Тәеулсіздік 118, французский культурный центр,2 этаж", "Костанай", new Guid("a9091b3e-61d0-4166-8450-f2c87accbf93") },
                    { new Guid("df1cfdc6-0f30-4d31-9c68-57997d7cb90f"), "ул.Байконурова 123", "г.Жезказган", new Guid("c1e223eb-8ee9-4bff-a6ae-1dade6d20039") },
                    { new Guid("05b65aed-3b4b-4e1b-b56f-467d5ac87f5b"), "Ораз Жумашев 29", "Алматинская обл., Ақсуский район, с. Ильяс Жансугуров", new Guid("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    { new Guid("4cd9c01f-2f56-497c-992e-e3b4f64323dd"), "ул.Наурыз 62 село Шамалган школа Ушконыр", "Алм. Область Карасайский район Шамалган", new Guid("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    { new Guid("a59b361a-21d7-44b1-9056-29ec34b95e0b"), "город Есик ул Райымбека 44 школа Райымбек батыра", "Алм. Область Енбекшиказахский  район Есик", new Guid("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    { new Guid("c564c92c-8780-495c-ad0d-63d93ee5d9b4"), "улица Кастеева 12", "Алм. Область Панфиловский  район Жаркент", new Guid("5fe90a81-e723-43b7-9613-2fdcb9f4585e") },
                    { new Guid("430127c8-68c1-4573-b50a-e67d42b652f2"), "Алматинский р-н, ул.Арынова 1", "Актобе", new Guid("ea70f091-1d51-4477-9d4f-8879103bb8f7") },
                    { new Guid("887f862e-8c48-4f56-a455-de4739f0b8a2"), "ул. Д.Қонаев  24", "п.Қарауылкелді ", new Guid("ea70f091-1d51-4477-9d4f-8879103bb8f7") },
                    { new Guid("b878ec9f-6221-4b39-bb97-60cb68c8b62d"), "ул.Ә.Жангелдин 89А", "г.Шалкар ", new Guid("ea70f091-1d51-4477-9d4f-8879103bb8f7") },
                    { new Guid("20b9250c-7354-4d03-94dc-a8fff5dda7e2"), "ул. Акана-серэ, 24", "Кокшетау", new Guid("017567f4-e687-446d-a2d7-0f2df5798200") },
                    { new Guid("5abc87e4-e038-4b79-8ecf-bce61c2ecffb"), "Жеңіс 86", "Атбасарский район, Атбасар ", new Guid("017567f4-e687-446d-a2d7-0f2df5798200") },
                    { new Guid("a3bc3719-694c-427f-a605-6ba6b498ab4a"), "Нурмагамбетова 144", "Акколский район. Акколь", new Guid("017567f4-e687-446d-a2d7-0f2df5798200") },
                    { new Guid("f5690c7e-7c00-499e-a349-65e2b36dce1e"), "ул. Баймуханова, 45а, АИНГ", "Атырау", new Guid("9b7648ca-3989-4ddd-9356-00c20324f6b8") },
                    { new Guid("f3311eee-8103-4312-8235-8f0030a3ed51"), "улица Жандосова, 40/1", "Шымкент 2", new Guid("b303ac57-8506-4a4d-83c4-f649e2170e3e") },
                    { new Guid("21b7c4c4-a328-4fbd-b253-35229b08c1c4"), "улица 199, дом 1", "г. Кульсары", new Guid("9b7648ca-3989-4ddd-9356-00c20324f6b8") },
                    { new Guid("4a251f8c-f0cf-4813-9205-068fffe744a5"), "Н.Назарбаева 86/2 (КАСУ)", "Усть-Каменогорск", new Guid("b99e5dab-18e7-496d-8d00-aab6ebe1754c") }
                });

            migrationBuilder.InsertData(
                table: "Offices",
                columns: new[] { "Id", "Address", "Name", "RegionId" },
                values: new object[,]
                {
                    { new Guid("0b4d6ca7-9eb2-4c61-a4e0-2eb68a9c0d7e"), "ул. Танирбергенова, 1", "Семей", new Guid("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    { new Guid("6ef5ac8e-275b-488e-bcb4-ca9c067e59a4"), "ул. Шулятикова 64", "с.Улкен Нарын", new Guid("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    { new Guid("df370a80-c331-4b21-8cb2-22ed9d41ddac"), "ул.Муканова 20", "с. Аксуат ", new Guid("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    { new Guid("253a277c-40a1-4bbb-b051-f275b3540b9f"), "ул.Кабанбай Батыра, строение 5/1", "г.Аягоз ", new Guid("b99e5dab-18e7-496d-8d00-aab6ebe1754c") },
                    { new Guid("8781c7d6-f221-41f0-a150-300f6504bc5a"), "Тауке хана 24", "Тараз", new Guid("95069bca-bf8c-4371-b070-b44c288c248f") },
                    { new Guid("a6db74eb-a0b1-405a-b395-c9871ec75bc3"), "Шуский район село Толеби ул.Кунаева 92 ", "Шуский район село Толеби ", new Guid("95069bca-bf8c-4371-b070-b44c288c248f") },
                    { new Guid("9ab64280-b541-4b0a-9c90-15d7c7ecaaf2"), "ТЦ \"Форум\", 4 этаж, ул. Ж. Молдагалиева 18", "Уральск", new Guid("ed62791f-83c5-4219-9dec-19f21be1605f") },
                    { new Guid("5d9f00a4-c826-4403-92d8-15bf0291c237"), "ул. Молодежная 29", "г. Аксай", new Guid("ed62791f-83c5-4219-9dec-19f21be1605f") },
                    { new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca"), "Ул.Казахстанская 15А", "Караганда", new Guid("c1e223eb-8ee9-4bff-a6ae-1dade6d20039") },
                    { new Guid("ae8750ad-41a5-48b5-952a-66913f648382"), "ул.Караменде би, 17", "г.Балхаш", new Guid("c1e223eb-8ee9-4bff-a6ae-1dade6d20039") },
                    { new Guid("32ed3077-8c02-41c3-bc85-bcebcb0cca20"), "ул. Отан дом 17 (Курмангазинский агро-технический колледж)", "Ганюшкино", new Guid("9b7648ca-3989-4ddd-9356-00c20324f6b8") },
                    { new Guid("7361c396-f717-4bc1-aebd-3ff78609be78"), "ул. Мухамед Хайдар Дулати 225 (3-4 этаж)", "Шымкент 3", new Guid("b303ac57-8506-4a4d-83c4-f649e2170e3e") }
                });

            migrationBuilder.InsertData(
                table: "Classrooms",
                columns: new[] { "Id", "Capacity", "OfficeId" },
                values: new object[,]
                {
                    { new Guid("1c207dfe-50d5-4e63-80d8-b79493a0672d"), 26, new Guid("35b7598d-28f1-4d12-aaa1-95aca831950c") },
                    { new Guid("8316ca76-af39-4c44-90a7-205d62453500"), 18, new Guid("b5ce5731-2299-4090-9b61-df82017a24f5") },
                    { new Guid("834aa728-71c4-403b-bf59-65b5dfe1111c"), 8, new Guid("b5ce5731-2299-4090-9b61-df82017a24f5") },
                    { new Guid("8d70b14d-0ca5-4259-8302-343d747ee662"), 42, new Guid("b5ce5731-2299-4090-9b61-df82017a24f5") },
                    { new Guid("afef1eb7-5e0c-4a97-a97b-27c68ed1125c"), 56, new Guid("1a0e2a97-a8a2-45ee-accf-2a04714cf3a2") },
                    { new Guid("beca5304-7491-4866-b4ad-b950c66e0ed5"), 24, new Guid("1a0e2a97-a8a2-45ee-accf-2a04714cf3a2") },
                    { new Guid("893e1062-9de4-406c-a8a7-13d5e072ed3f"), 19, new Guid("bbb4d447-5df7-4622-9194-06d900ce7934") },
                    { new Guid("9f3631cc-de01-44d7-b8f7-2cb44bd3a511"), 18, new Guid("bbb4d447-5df7-4622-9194-06d900ce7934") },
                    { new Guid("92f4ee21-9580-4f85-849c-4d4330cae9ab"), 23, new Guid("bbb4d447-5df7-4622-9194-06d900ce7934") },
                    { new Guid("ef47f0c6-0ed9-4a09-b133-18a2587776c5"), 82, new Guid("b5ce5731-2299-4090-9b61-df82017a24f5") },
                    { new Guid("ccb9ca07-1a56-4d85-925f-e4d89b999ec6"), 41, new Guid("ed58427d-f5b3-46e2-b83d-d9bc83d3ef75") },
                    { new Guid("64a498b0-4555-41b3-b19f-86415645960f"), 20, new Guid("27936879-a81b-4970-9633-11d41777db88") },
                    { new Guid("a93ff9fb-325d-456b-aa0b-8c2524fe0d08"), 10, new Guid("27936879-a81b-4970-9633-11d41777db88") },
                    { new Guid("6fa07484-f185-4449-a2c4-81e8039deec2"), 10, new Guid("27936879-a81b-4970-9633-11d41777db88") },
                    { new Guid("a078c43e-7d4f-4ded-8ec1-1e72bb0d90cf"), 20, new Guid("36a26cbd-5ae0-4356-936f-f6cdb030ea1f") },
                    { new Guid("c3d71b01-783f-40e3-b65d-9e89596cec2e"), 20, new Guid("36a26cbd-5ae0-4356-936f-f6cdb030ea1f") },
                    { new Guid("9e9c4aea-e335-4a0e-b060-b2bc8a110005"), 75, new Guid("2f625dc6-592c-4f76-a771-59d01669e3da") },
                    { new Guid("25e5f9e7-35a0-4b3e-99fc-2f0a60b90610"), 25, new Guid("2f625dc6-592c-4f76-a771-59d01669e3da") },
                    { new Guid("920f8e1b-d916-4fcc-8eb8-960f3f583ccf"), 36, new Guid("80ec8904-8b03-4a15-9684-8a8bf48a28af") },
                    { new Guid("645193c6-62ba-4327-b313-6959ca614a2f"), 21, new Guid("ed58427d-f5b3-46e2-b83d-d9bc83d3ef75") },
                    { new Guid("ecf6eef2-0901-41e3-9979-34bcf07459ef"), 24, new Guid("80ec8904-8b03-4a15-9684-8a8bf48a28af") },
                    { new Guid("e7ee9805-27f0-4140-a2ba-4fdf18cbc5b3"), 34, new Guid("9e8b87b9-0eef-4ae5-8a00-69b6fe84990c") },
                    { new Guid("57c86722-c736-4e9d-9352-92e183c098d7"), 33, new Guid("9e8b87b9-0eef-4ae5-8a00-69b6fe84990c") },
                    { new Guid("9609f7c6-6902-4f2a-b773-21151f50c2e2"), 10, new Guid("88b4b9bd-82f2-49ba-b168-850809a54cbe") },
                    { new Guid("722f6cd9-cad6-4311-91c8-be2cb88a3970"), 55, new Guid("16b937e8-91b7-4b4b-9a77-e55e615d9c54") },
                    { new Guid("8c7b5b39-4761-4756-a614-f808f10d73bd"), 45, new Guid("16b937e8-91b7-4b4b-9a77-e55e615d9c54") },
                    { new Guid("675bba2a-589f-41c6-b9d9-dea688dd02f8"), 34, new Guid("5b0e85d0-f832-4d5e-8887-8d98ee5190de") },
                    { new Guid("b3baf9b0-ccd8-4135-b521-c75827fa565c"), 33, new Guid("5b0e85d0-f832-4d5e-8887-8d98ee5190de") },
                    { new Guid("ac661c25-504e-412f-963c-8f331cc2ea19"), 33, new Guid("5b0e85d0-f832-4d5e-8887-8d98ee5190de") },
                    { new Guid("69d1245a-f58e-4398-a313-ecc4e52b1e12"), 24, new Guid("1d184008-f44c-46a4-99ec-762ba4ac5ee1") },
                    { new Guid("76e0da54-e232-4035-b8a3-718069528636"), 24, new Guid("1d184008-f44c-46a4-99ec-762ba4ac5ee1") },
                    { new Guid("3ccf7777-ac22-4b91-b5af-79236581b139"), 33, new Guid("9e8b87b9-0eef-4ae5-8a00-69b6fe84990c") },
                    { new Guid("40bcfb1b-365a-4bb6-b08b-8b124e732320"), 32, new Guid("1d184008-f44c-46a4-99ec-762ba4ac5ee1") },
                    { new Guid("a984cadc-0b9f-4d92-84b7-49c2ff2da09f"), 30, new Guid("ee8c293b-23f0-4f0c-8d96-fc8dc67c6e3c") },
                    { new Guid("414fd851-4f64-4d91-b154-05005cf1b06f"), 30, new Guid("ee8c293b-23f0-4f0c-8d96-fc8dc67c6e3c") },
                    { new Guid("928c461e-1fd0-41b0-8161-d6092457c0d5"), 30, new Guid("ee8c293b-23f0-4f0c-8d96-fc8dc67c6e3c") },
                    { new Guid("85c28af7-f216-4d55-97cc-8b3118b34f37"), 30, new Guid("ee8c293b-23f0-4f0c-8d96-fc8dc67c6e3c") },
                    { new Guid("51faf0da-cc18-4170-8d71-a907c1e02ba2"), 30, new Guid("ee8c293b-23f0-4f0c-8d96-fc8dc67c6e3c") },
                    { new Guid("dcf63bf3-1c6c-41db-b3eb-5a250dfc371f"), 20, new Guid("0a667344-4d06-497b-b224-d31dac9cb5e7") },
                    { new Guid("a88dcb8d-f243-46f7-bb6d-2afe53b2ac46"), 20, new Guid("0a667344-4d06-497b-b224-d31dac9cb5e7") },
                    { new Guid("5657a507-c150-4e0b-9b7e-fe28f56c6a3a"), 20, new Guid("0a667344-4d06-497b-b224-d31dac9cb5e7") },
                    { new Guid("6acf2b9f-ed8a-4029-8839-435be3ebec84"), 20, new Guid("1d184008-f44c-46a4-99ec-762ba4ac5ee1") }
                });

            migrationBuilder.InsertData(
                table: "Classrooms",
                columns: new[] { "Id", "Capacity", "OfficeId" },
                values: new object[,]
                {
                    { new Guid("204e7905-3713-4ebf-9c50-dfb4907a9f86"), 34, new Guid("80ec8904-8b03-4a15-9684-8a8bf48a28af") },
                    { new Guid("b774c5a6-34a9-474f-9761-d049734c5d2c"), 34, new Guid("80ec8904-8b03-4a15-9684-8a8bf48a28af") },
                    { new Guid("c83b381d-e8be-4596-aa0a-798ce4180580"), 32, new Guid("80ec8904-8b03-4a15-9684-8a8bf48a28af") },
                    { new Guid("8591b40d-7460-4289-8135-10731176b557"), 20, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("ecf36d8e-856d-4314-9ce2-c4c0c334620e"), 18, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("ddd552f4-6d4a-4150-a3ae-35a00db7ddc8"), 18, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("e47b9626-aff0-4ae0-8f9b-9cdbfff57ba8"), 18, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("4799cfba-54ba-41c2-8e01-fd3e57fe8357"), 19, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("2ab9deba-493f-41e9-a8b3-30f6e4010215"), 17, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("39f81ac0-835c-4556-8851-06c9b6fdc6a3"), 20, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("dbdc2d19-999f-4665-b2b6-1d2dc3d68850"), 20, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("6e841f90-b947-4966-bd6c-efa0050a0d68"), 330, new Guid("b238d135-dd85-47fa-9764-a2e9172f5f97") },
                    { new Guid("e1d6517e-0d25-4012-ab3c-92c335572a47"), 18, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("0ad254ac-64ea-416d-8f44-c58af57b034d"), 18, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("4209a10a-543e-4c30-8ed4-25e6bd80ae82"), 38, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("03aa4cc1-bfb1-45c2-8b4e-9f34a73cb818"), 31, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("1f2f2c35-a2eb-468a-a448-ff96104bc531"), 20, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("f14360db-a62c-42f6-b21d-22ae92a7882d"), 20, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("8f0ef281-9d14-456a-a3e6-b8158f457c71"), 20, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("96ba790d-710f-405f-ab4e-c9007b3d4e38"), 20, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("efe9b06d-008b-460a-ad35-faf4740f4243"), 18, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("f227415a-6e15-40ef-88bd-ae504b0b6118"), 20, new Guid("2f2bdbd3-6de5-443b-a522-32998d963a25") },
                    { new Guid("c76e9c08-94d5-439d-ae66-eaf378103cc3"), 23, new Guid("8e3ef246-7706-4a5f-afa8-b2feb935f64d") },
                    { new Guid("0ee0fa06-77af-4b41-bde7-62b4122e36ea"), 23, new Guid("8e3ef246-7706-4a5f-afa8-b2feb935f64d") },
                    { new Guid("aa3ca6b5-e6e3-4fe5-9e04-f26494009275"), 21, new Guid("8e3ef246-7706-4a5f-afa8-b2feb935f64d") },
                    { new Guid("79915120-b075-42c8-9cdf-22e4bdacd7bc"), 60, new Guid("4b8c3b76-a91f-479f-9ef3-f25ca981e218") },
                    { new Guid("b4aa6eb4-77b3-498e-90ff-a1025a7ec1d9"), 30, new Guid("cde7f22c-74a2-4db1-a26f-9ba2fe016779") },
                    { new Guid("27148735-3e82-4c7a-bb47-58ee9398abe7"), 24, new Guid("cde7f22c-74a2-4db1-a26f-9ba2fe016779") },
                    { new Guid("91831722-419b-4c5d-a37d-1799140571fa"), 25, new Guid("cde7f22c-74a2-4db1-a26f-9ba2fe016779") },
                    { new Guid("053935f4-dc74-44f8-92f7-938a3a9282f9"), 25, new Guid("cde7f22c-74a2-4db1-a26f-9ba2fe016779") },
                    { new Guid("db2ff79b-ca73-4fa3-ab65-8f8181251c07"), 25, new Guid("cde7f22c-74a2-4db1-a26f-9ba2fe016779") },
                    { new Guid("5e0704ac-97a0-47f9-8a79-e41b9016c946"), 31, new Guid("cde7f22c-74a2-4db1-a26f-9ba2fe016779") },
                    { new Guid("2b8241bc-9c4c-4b2d-b331-d422d556323b"), 19, new Guid("d3c368bc-cc3c-48ec-a227-dce8a1a65913") },
                    { new Guid("f01bf0c8-6921-4aa0-902e-b94742805f6a"), 19, new Guid("d3c368bc-cc3c-48ec-a227-dce8a1a65913") },
                    { new Guid("6e10d7e7-0f9a-4cbb-bbef-652c04849cfd"), 22, new Guid("d3c368bc-cc3c-48ec-a227-dce8a1a65913") },
                    { new Guid("ad69b3ec-38f4-4680-9553-4de5f7fe4c6f"), 34, new Guid("04097107-c061-4d50-874d-f81d0c25db80") },
                    { new Guid("2f768aa4-e498-4839-bbd9-e3f23df0fb21"), 28, new Guid("04097107-c061-4d50-874d-f81d0c25db80") },
                    { new Guid("e09e7287-815d-486b-9524-6e905ab6c31b"), 24, new Guid("04097107-c061-4d50-874d-f81d0c25db80") },
                    { new Guid("95960658-622b-405a-87bc-3c1c3e321149"), 27, new Guid("04097107-c061-4d50-874d-f81d0c25db80") },
                    { new Guid("3f11dc69-93ce-4486-82fd-be85d8dd945b"), 27, new Guid("04097107-c061-4d50-874d-f81d0c25db80") },
                    { new Guid("911f074e-ec83-439f-bac6-99cac09de50a"), 150, new Guid("ea5ab2e0-f21e-483e-9181-abe8ffe23b77") },
                    { new Guid("bc2d4392-02d4-4859-8f52-e70c1c6e7b83"), 106, new Guid("ea5ab2e0-f21e-483e-9181-abe8ffe23b77") }
                });

            migrationBuilder.InsertData(
                table: "Classrooms",
                columns: new[] { "Id", "Capacity", "OfficeId" },
                values: new object[,]
                {
                    { new Guid("7c3c52ee-9af9-4908-bb8d-d08f57007d85"), 44, new Guid("ea5ab2e0-f21e-483e-9181-abe8ffe23b77") },
                    { new Guid("d7cfded9-47ae-40a9-bae7-a86552997f27"), 25, new Guid("8e3ef246-7706-4a5f-afa8-b2feb935f64d") },
                    { new Guid("c5e6265c-a997-4c95-b253-397af5c78957"), 24, new Guid("88b4b9bd-82f2-49ba-b168-850809a54cbe") },
                    { new Guid("9ead9aed-e5e5-4d19-a2b0-9093bd370164"), 115, new Guid("f3311eee-8103-4312-8235-8f0030a3ed51") },
                    { new Guid("f939e3e3-ae9c-4ade-8876-5dc7b1b598ef"), 34, new Guid("88b4b9bd-82f2-49ba-b168-850809a54cbe") },
                    { new Guid("ffa56142-8034-404f-b319-b847cdb6d6ec"), 73, new Guid("88b4b9bd-82f2-49ba-b168-850809a54cbe") },
                    { new Guid("9fd83e6d-8d45-4fc2-a26c-723bb175edf0"), 60, new Guid("887f862e-8c48-4f56-a455-de4739f0b8a2") },
                    { new Guid("67f621a9-5b82-4fda-be27-8f0499cef85f"), 26, new Guid("b878ec9f-6221-4b39-bb97-60cb68c8b62d") },
                    { new Guid("2c31e69d-6d10-4b8b-9aa8-da168344a571"), 26, new Guid("b878ec9f-6221-4b39-bb97-60cb68c8b62d") },
                    { new Guid("49bceffb-d02c-4cff-ad55-70de1fbb89e8"), 28, new Guid("b878ec9f-6221-4b39-bb97-60cb68c8b62d") },
                    { new Guid("541500b0-c129-4697-af68-b860890de8cd"), 10, new Guid("20b9250c-7354-4d03-94dc-a8fff5dda7e2") },
                    { new Guid("6eb78025-64e1-4ddb-a026-c2e18063d2b4"), 17, new Guid("20b9250c-7354-4d03-94dc-a8fff5dda7e2") },
                    { new Guid("f8a86a0f-ccd3-424a-9242-d1ce8f305868"), 17, new Guid("20b9250c-7354-4d03-94dc-a8fff5dda7e2") },
                    { new Guid("a3ec4fe5-88d8-4945-8979-6f774d9b9af0"), 18, new Guid("20b9250c-7354-4d03-94dc-a8fff5dda7e2") },
                    { new Guid("a3cf1687-73cd-4e40-aa1a-45aa670ad5ba"), 40, new Guid("430127c8-68c1-4573-b50a-e67d42b652f2") },
                    { new Guid("ddf1bfba-d9e6-417d-b479-0f0494d858a7"), 15, new Guid("20b9250c-7354-4d03-94dc-a8fff5dda7e2") },
                    { new Guid("7d3895fc-9fe7-4a06-a8fd-6674fe75c4bf"), 24, new Guid("5abc87e4-e038-4b79-8ecf-bce61c2ecffb") },
                    { new Guid("446f4e4f-74a5-408a-8531-45abd24bc424"), 36, new Guid("5abc87e4-e038-4b79-8ecf-bce61c2ecffb") },
                    { new Guid("514c7289-e835-43d3-b48f-26b998296751"), 30, new Guid("a3bc3719-694c-427f-a605-6ba6b498ab4a") },
                    { new Guid("fb30f6e1-7b6a-4f2c-ab8f-14b5ea0d7dc2"), 30, new Guid("a3bc3719-694c-427f-a605-6ba6b498ab4a") },
                    { new Guid("cec691ef-1e78-4d86-a1c0-f015dfcf6122"), 32, new Guid("f5690c7e-7c00-499e-a349-65e2b36dce1e") },
                    { new Guid("148dceba-cc41-4317-bfa6-94642ca112a0"), 12, new Guid("f5690c7e-7c00-499e-a349-65e2b36dce1e") },
                    { new Guid("910dfbf0-c8ba-4852-b2c1-43e5f9df8bdd"), 40, new Guid("f5690c7e-7c00-499e-a349-65e2b36dce1e") },
                    { new Guid("32edd362-0590-468a-90f4-2cfca95555a9"), 33, new Guid("f5690c7e-7c00-499e-a349-65e2b36dce1e") },
                    { new Guid("bef52a70-d87c-4c59-9667-edcb619da523"), 23, new Guid("20b9250c-7354-4d03-94dc-a8fff5dda7e2") },
                    { new Guid("07aca76c-d9f5-41c3-88d5-76c8a6169d97"), 24, new Guid("f5690c7e-7c00-499e-a349-65e2b36dce1e") },
                    { new Guid("fa1c3967-c8e9-4150-8524-a405140ed341"), 19, new Guid("430127c8-68c1-4573-b50a-e67d42b652f2") },
                    { new Guid("0e0fbdfb-46a2-4cf5-a9ac-759a4363060b"), 19, new Guid("430127c8-68c1-4573-b50a-e67d42b652f2") },
                    { new Guid("fcec6820-6ea6-412f-8b7b-8994cecfb458"), 28, new Guid("35b7598d-28f1-4d12-aaa1-95aca831950c") },
                    { new Guid("3ffaf64e-a437-4f3f-a4d8-61928c26eac9"), 30, new Guid("35b7598d-28f1-4d12-aaa1-95aca831950c") },
                    { new Guid("2f066448-6a9a-4dca-ac08-bd4ee1410ccd"), 22, new Guid("35b7598d-28f1-4d12-aaa1-95aca831950c") },
                    { new Guid("ac90b912-697f-49f2-8846-56d769d861ca"), 44, new Guid("35b7598d-28f1-4d12-aaa1-95aca831950c") },
                    { new Guid("8ef7c2bc-38e6-4a88-95f9-b9bba01359da"), 33, new Guid("05b65aed-3b4b-4e1b-b56f-467d5ac87f5b") },
                    { new Guid("b3df300f-0a7a-4a02-91a4-a13de8e561ed"), 33, new Guid("05b65aed-3b4b-4e1b-b56f-467d5ac87f5b") },
                    { new Guid("003ebaa2-3383-4c26-9203-51142693c3fe"), 34, new Guid("05b65aed-3b4b-4e1b-b56f-467d5ac87f5b") },
                    { new Guid("052a8724-b168-4cb2-bf1a-c26fb4a10ef3"), 30, new Guid("4cd9c01f-2f56-497c-992e-e3b4f64323dd") },
                    { new Guid("654c48b9-d486-48ce-92fa-a01dbce432aa"), 36, new Guid("430127c8-68c1-4573-b50a-e67d42b652f2") },
                    { new Guid("6b810101-42d4-4b57-bcbd-c75247211c39"), 30, new Guid("4cd9c01f-2f56-497c-992e-e3b4f64323dd") },
                    { new Guid("5e6ada0f-2de7-45ca-b54d-48f0bd7bdd04"), 30, new Guid("4cd9c01f-2f56-497c-992e-e3b4f64323dd") },
                    { new Guid("c1c2646d-af55-4aa3-acd5-e018d54f8fba"), 30, new Guid("a59b361a-21d7-44b1-9056-29ec34b95e0b") },
                    { new Guid("e666cfe5-8d49-4840-8752-4272550e3ebb"), 30, new Guid("a59b361a-21d7-44b1-9056-29ec34b95e0b") },
                    { new Guid("14e6a26d-1522-47f2-a30e-5fdd71dd0048"), 30, new Guid("a59b361a-21d7-44b1-9056-29ec34b95e0b") }
                });

            migrationBuilder.InsertData(
                table: "Classrooms",
                columns: new[] { "Id", "Capacity", "OfficeId" },
                values: new object[,]
                {
                    { new Guid("d605ca8b-e613-45b5-93bc-46c26cea6e90"), 30, new Guid("a59b361a-21d7-44b1-9056-29ec34b95e0b") },
                    { new Guid("f2c9814f-07da-4ef1-b7cf-d5cdb44759db"), 59, new Guid("c564c92c-8780-495c-ad0d-63d93ee5d9b4") },
                    { new Guid("28c52b80-d333-4298-a362-a4abea85908f"), 41, new Guid("c564c92c-8780-495c-ad0d-63d93ee5d9b4") },
                    { new Guid("d4c72bc4-3cc6-40ec-8b2e-fa541e66e9e7"), 36, new Guid("430127c8-68c1-4573-b50a-e67d42b652f2") },
                    { new Guid("6e907c3b-5537-410f-a494-1cd426386173"), 30, new Guid("4cd9c01f-2f56-497c-992e-e3b4f64323dd") },
                    { new Guid("d35eab4e-84da-4288-b8b9-a0653774c666"), 14, new Guid("f5690c7e-7c00-499e-a349-65e2b36dce1e") },
                    { new Guid("de9fa4f5-0247-4bc7-88ed-102fa4003cb0"), 30, new Guid("21b7c4c4-a328-4fbd-b253-35229b08c1c4") },
                    { new Guid("fb72f8a1-db28-4633-a1a5-ccd5566b0748"), 30, new Guid("21b7c4c4-a328-4fbd-b253-35229b08c1c4") },
                    { new Guid("cfcb04ca-8484-46db-be87-5424a31147a3"), 15, new Guid("5d9f00a4-c826-4403-92d8-15bf0291c237") },
                    { new Guid("1b7bcac9-d294-4d79-a946-ddf02bff52d6"), 9, new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca") },
                    { new Guid("18207aa6-79de-4f53-b3b0-ab338dd286b2"), 18, new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca") },
                    { new Guid("cb274881-2b1c-4cf6-aba7-e3b5c837dd31"), 9, new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca") },
                    { new Guid("e74d36f5-6848-4dc0-85cf-ae5384812957"), 9, new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca") },
                    { new Guid("6e5213fd-3b5e-4aff-91c7-0cf65e10857d"), 9, new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca") },
                    { new Guid("8ee699a3-c278-4195-881e-8127da67c963"), 47, new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca") },
                    { new Guid("523d4c53-21ef-46df-a4c3-1b4a0901bb17"), 24, new Guid("631dbe6f-b02b-4324-aca9-edd8b69b70ca") },
                    { new Guid("3d7c8bf1-d926-40de-ac86-97a4675a4097"), 25, new Guid("5d9f00a4-c826-4403-92d8-15bf0291c237") },
                    { new Guid("45b16265-b875-413c-aee1-2550878ba64d"), 30, new Guid("ae8750ad-41a5-48b5-952a-66913f648382") },
                    { new Guid("735d76dd-9b9d-4e3d-9e1c-e9c5af642cd5"), 30, new Guid("df1cfdc6-0f30-4d31-9c68-57997d7cb90f") },
                    { new Guid("442864ab-3196-4c76-a14c-10d250492b82"), 30, new Guid("df1cfdc6-0f30-4d31-9c68-57997d7cb90f") },
                    { new Guid("cc5b90fc-3920-486f-9b17-e5c8baf9f171"), 85, new Guid("c34ccf6a-d27e-4a17-9868-99b855abed0b") },
                    { new Guid("204b3a82-e5e8-4f69-bf72-7f0d55b8ad74"), 15, new Guid("c34ccf6a-d27e-4a17-9868-99b855abed0b") },
                    { new Guid("1e8715f9-6d3e-40f5-96e7-dc15c5cb6783"), 35, new Guid("e6323325-bd98-4306-b4c3-ca8cea161ca2") },
                    { new Guid("b3caeebf-4ce1-4afd-9f51-6179aefaaa89"), 25, new Guid("e6323325-bd98-4306-b4c3-ca8cea161ca2") },
                    { new Guid("a25075ef-b018-419c-9149-b500496499c2"), 30, new Guid("a09c37a8-74f4-4a82-8282-6fc1ba2cb4ef") },
                    { new Guid("e1432408-9725-4f81-8461-26d865d48761"), 30, new Guid("a09c37a8-74f4-4a82-8282-6fc1ba2cb4ef") },
                    { new Guid("f6a8653a-150a-477e-8072-5e2ce4a7d2bb"), 30, new Guid("ae8750ad-41a5-48b5-952a-66913f648382") },
                    { new Guid("113678d9-d49c-43cd-82f7-0793fe69f884"), 68, new Guid("9ab64280-b541-4b0a-9c90-15d7c7ecaaf2") },
                    { new Guid("4dc76eb3-186a-44e5-918f-3150bad80e26"), 21, new Guid("9ab64280-b541-4b0a-9c90-15d7c7ecaaf2") },
                    { new Guid("bd715e81-95b1-4de8-a74d-2055a2058264"), 31, new Guid("9ab64280-b541-4b0a-9c90-15d7c7ecaaf2") },
                    { new Guid("a7e3b6c8-f356-4995-a918-04b300ae71a3"), 20, new Guid("21b7c4c4-a328-4fbd-b253-35229b08c1c4") },
                    { new Guid("3c64bf92-8abe-45a0-8c8a-9a213fe0346b"), 25, new Guid("32ed3077-8c02-41c3-bc85-bcebcb0cca20") },
                    { new Guid("992050a2-04e4-4889-a75f-1ca660f1f5c3"), 25, new Guid("32ed3077-8c02-41c3-bc85-bcebcb0cca20") },
                    { new Guid("75ea6c82-fbf0-4c67-92d6-4edaf98cecca"), 90, new Guid("4a251f8c-f0cf-4813-9205-068fffe744a5") },
                    { new Guid("c66bc4a3-757c-4d9d-9891-a00c07503391"), 36, new Guid("4a251f8c-f0cf-4813-9205-068fffe744a5") },
                    { new Guid("8ad25c04-d796-4b16-b1c0-5ab75163b305"), 24, new Guid("4a251f8c-f0cf-4813-9205-068fffe744a5") },
                    { new Guid("1996e1b1-2c03-453f-b68b-67aca3efd2e3"), 25, new Guid("0b4d6ca7-9eb2-4c61-a4e0-2eb68a9c0d7e") },
                    { new Guid("ca0b82e2-b190-4c12-b4c0-761581352e64"), 75, new Guid("0b4d6ca7-9eb2-4c61-a4e0-2eb68a9c0d7e") },
                    { new Guid("253d22fa-32dc-4324-b33b-212c15569d49"), 32, new Guid("6ef5ac8e-275b-488e-bcb4-ca9c067e59a4") },
                    { new Guid("17f5795c-d05a-41ae-809d-dfa50e07edf2"), 28, new Guid("6ef5ac8e-275b-488e-bcb4-ca9c067e59a4") },
                    { new Guid("4bde146e-5fd5-43f2-95f3-c13f9f26705f"), 20, new Guid("6ef5ac8e-275b-488e-bcb4-ca9c067e59a4") },
                    { new Guid("e9737d7d-3797-4679-945b-5baba23e7e04"), 29, new Guid("df370a80-c331-4b21-8cb2-22ed9d41ddac") }
                });

            migrationBuilder.InsertData(
                table: "Classrooms",
                columns: new[] { "Id", "Capacity", "OfficeId" },
                values: new object[,]
                {
                    { new Guid("ea0dba11-2366-4671-8507-88bfc898546a"), 31, new Guid("df370a80-c331-4b21-8cb2-22ed9d41ddac") },
                    { new Guid("9a4eb32f-7430-4d3a-8b64-9ebcc91ef609"), 26, new Guid("253a277c-40a1-4bbb-b051-f275b3540b9f") },
                    { new Guid("458f6c05-48ad-4942-80d2-dbd8fd9c8f46"), 37, new Guid("253a277c-40a1-4bbb-b051-f275b3540b9f") },
                    { new Guid("4097f99f-8690-46d6-9868-0fa137c475c0"), 27, new Guid("253a277c-40a1-4bbb-b051-f275b3540b9f") },
                    { new Guid("2d4fa8df-541e-4fc8-936c-86dbed3af107"), 210, new Guid("8781c7d6-f221-41f0-a150-300f6504bc5a") },
                    { new Guid("a79eb3b4-f0b4-491e-a2d3-b952bb5d97c5"), 100, new Guid("a6db74eb-a0b1-405a-b395-c9871ec75bc3") },
                    { new Guid("7f536553-c0ca-48ce-a7a8-c5b83165c5d7"), 20, new Guid("9ab64280-b541-4b0a-9c90-15d7c7ecaaf2") },
                    { new Guid("d35bfa99-71e2-4262-8fc0-736356e6522b"), 34, new Guid("88b4b9bd-82f2-49ba-b168-850809a54cbe") },
                    { new Guid("1dd281b7-3f8c-4194-89b2-e3b57ffb2f86"), 120, new Guid("7361c396-f717-4bc1-aebd-3ff78609be78") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Actions_ExamId",
                table: "Actions",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_Actions_ProposalId",
                table: "Actions",
                column: "ProposalId");

            migrationBuilder.CreateIndex(
                name: "IX_Classrooms_OfficeId",
                table: "Classrooms",
                column: "OfficeId");

            migrationBuilder.CreateIndex(
                name: "IX_ExamOffice_OfficesId",
                table: "ExamOffice",
                column: "OfficesId");

            migrationBuilder.CreateIndex(
                name: "IX_Offices_RegionId",
                table: "Offices",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Proposals_ClassroomId",
                table: "Proposals",
                column: "ClassroomId");

            migrationBuilder.CreateIndex(
                name: "IX_Proposals_SenderId",
                table: "Proposals",
                column: "SenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Regions_Name",
                table: "Regions",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Login",
                table: "Users",
                column: "Login",
                unique: true,
                filter: "[Login] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RegionId",
                table: "Users",
                column: "RegionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Actions");

            migrationBuilder.DropTable(
                name: "ExamOffice");

            migrationBuilder.DropTable(
                name: "Proposals");

            migrationBuilder.DropTable(
                name: "Exams");

            migrationBuilder.DropTable(
                name: "Classrooms");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Offices");

            migrationBuilder.DropTable(
                name: "Regions");
        }
    }
}
