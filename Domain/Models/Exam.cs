﻿using System;
using System.Collections.Generic;

namespace UPlanner.Domain.Models
{
    public class Exam
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public bool Mixable { get; set; }

        public ICollection<Office> Offices { get; set; }
    }
}
