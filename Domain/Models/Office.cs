﻿using System;
using System.Collections.Generic;

namespace UPlanner.Domain.Models
{
    public class Office
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }

        public Guid RegionId { get; set; }
        public Region Region { get; set; }

        public ICollection<Classroom> Classrooms { get; set; }
        public ICollection<Exam> Exams { get; set; }
    }
}
