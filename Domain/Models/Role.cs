﻿namespace UPlanner.Domain.Models
{
    public enum Role
    {
        Administrator,
        InternalClient,
        ExternalClient
    }
}
