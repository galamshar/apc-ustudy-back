﻿using System;
using System.Collections.Generic;

namespace UPlanner.Domain.Models
{
    public class PlanProposal
    {
        public Guid Id { get; set; }

        public Guid SenderId { get; set; }
        public User Sender { get; set; }

        public PlanProposalStatus Status { get; set; }
        public Guid ClassroomId { get; set; }
        public Classroom Classroom { get; set; }

        public ICollection<PlanAction> Actions { get; set; }
    }

    public enum PlanProposalStatus
    {
        Created,
        Approved,
        Rejected
    }
}
