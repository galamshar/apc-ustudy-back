﻿using System;
using System.Collections.Generic;

namespace UPlanner.Domain.Models
{
    public class Classroom
    {
        public Guid Id { get; set; }

        public int Capacity { get; set; }
        public ICollection<PlanProposal> Proposals { get; set; }
        public Guid OfficeId { get; set; }
        public Office Office { get; set; }
    }
}
