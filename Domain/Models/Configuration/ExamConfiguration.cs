﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPlanner.Domain.Models.Configuration
{
    public class ExamConfiguration : IEntityTypeConfiguration<Exam>
    {
        public void Configure(EntityTypeBuilder<Exam> builder)
        {
            builder.HasData(
                new Exam() { Id = Guid.NewGuid(), Name = "ЕНТ", Mixable = false},
                new Exam() { Id = Guid.NewGuid(), Name = "НКТ", Mixable = false },
                new Exam() { Id = Guid.NewGuid(), Name = "Вступительные в магистратуру", Mixable = true },
                new Exam() { Id = Guid.NewGuid(), Name = "Вступительные в докторантуру", Mixable = true }
                );
        }
    }
}
