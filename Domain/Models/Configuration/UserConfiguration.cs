﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPlanner.Domain.Models.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(u => u.RegionId).HasDefaultValue(Guid.Parse("00000000-0000-0000-0000-000000000001"));
            builder.HasData(
                new User() {Id = Guid.NewGuid(), Login = "admin", Role = Role.Administrator, PasswordHash = "qwerty123"}
                );
        }
    }
}
