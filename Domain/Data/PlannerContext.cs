﻿using Microsoft.EntityFrameworkCore;
using UPlanner.Domain.Models;
using UPlanner.Domain.Models.Configuration;

namespace UPlanner.Domain.Data
{
    public class PlannerContext : DbContext
    {
        public PlannerContext(DbContextOptions<PlannerContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; private set; }
        public DbSet<Region> Regions { get; private set; }
        public DbSet<Office> Offices { get; private set; }
        public DbSet<Classroom> Classrooms { get; private set; }
        public DbSet<Exam> Exams { get; private set; }
        public DbSet<PlanAction> Actions { get; private set; }
        public DbSet<PlanProposal> Proposals { get; private set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new RegionConfiguration());
            builder.ApplyConfiguration(new OfficeConfiguration());
            builder.ApplyConfiguration(new ClassroomConfiguration());
            builder.ApplyConfiguration(new ExamConfiguration());
            builder.Entity<User>(e =>
            {
                e.HasIndex(u => u.Login).IsUnique();
            });

            builder.Entity<Region>(e =>
            {
                e.HasIndex(r => r.Name).IsUnique();
            });
        }
    }
}
